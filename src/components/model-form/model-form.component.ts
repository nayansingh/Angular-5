import { Component,OnInit } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';

@Component({
  selector: 'model-form',
  templateUrl: './model-form.view.html',
})
export class ModelFormComponent implements OnInit{
  langs:string[]=[ 'Marathi','Gujrati','Konkani','Marwari','Punjabi' ]
  myform:FormGroup;

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.myform=new FormGroup({  
      // form group takes object as parameters, object has different form controls like email, password, language
      // FormGroup class is valid if each and every Form Control class inside it are valid
      fullName:new FormGroup({
        firstName:new FormControl(),
        lastName:new FormControl()
      }),

      email:new FormControl(),
      password:new FormControl(),
      language:new FormControl()
    })
  }
}
