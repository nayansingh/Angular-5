import { Component, OnDestroy } from '@angular/core';
import { map, filter, switchMap } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';
// import 'rxjs/add/observable/interval';

@Component({
  selector: 'pipes',
  templateUrl: './pipes.view.html',
})
export class PipesComponent implements OnDestroy{
  private date:Date=new Date();

  private jsonVal:Object={
    name:"Nayan Singh",
    games : {
      outdoor:"Cricket",
      indoor:"NFS most wanted"
    }
  };

  observData_without_async_pipe:number;
  observData_with_async_pipe:Observable<number>;
  
  subscriptionData:Subscription=null; 

  promiseData_Without_Async_Pipe:object; // resolve always returns object or equivalent form
  promiseData_with_async_pipe:Promise<object>; // type returned is promise which will return object
  // promise always returns object or equivalent form

  constructor() {
    this.getPromise().then( data => this.promiseData_Without_Async_Pipe = data);  
    // above code will fetch data returned from the promise 
    this.promiseData_with_async_pipe=this.getPromise();
    // above code will fetch the whole promise returned just to save time
    // this.subscribeObserv();
    
  }

  getPromise(){
    return new Promise( (res,rej)=>{
      setTimeout(()=>res("promise completed"),3000);
    });
  }

  // getObserv(){
  //   return interval(3000)
  //       .take(10)
  //       .map(data => data*data);
  // }

  // subscribeObserv(){
  //   this.subscriptionData=this.getObserv().subscribe(data=>this.observData_without_async_pipe =data);
  //   // a subscription is returned from a subscribe
  // }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    if(this.subscriptionData){
      this.subscriptionData.unsubscribe();
    }
  }

  
}
