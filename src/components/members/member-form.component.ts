import { Component, EventEmitter, Output, ViewEncapsulation } from '@angular/core';
import { Member } from './list.component';

@Component({
  selector: 'member-form',
  templateUrl:'member-form.view.html',
  styles:[
    `
    .card{
      background-color:gray
    }
    `
  ],

  // encapsulation:ViewEncapsulation.Native // it prevents to import other styles from external
  //bootstrap library and let us apply only our custom styles 

  // encapsulation:ViewEncapsulation.None // it applies our custom style globally
})
export class MemberFormComponent {
  @Output() addMember= new EventEmitter<Member>();  
  // createMember is an instance of EventEmitter
  // EventEmitter is a helper class which helps to emit events when something happens
  // type of thing to be output by createMember event emitter is Member

  add(name:string,post:string,age:number){
    this.addMember.emit(new Member(name,post,age));
  }

}
