import { Component } from '@angular/core';

export class Member{        // this is a domain model for EonicsMembersComponent below, that is
  // we have separated the data structure from the component 
  name:string;
  desig:string;
  age:number;
  hide:boolean;
  btnVal:string;
  
  constructor(name:string,desig:string,age:number) {
    this.name=name;
    this.desig=desig;
    this.age=age;
    this.hide=true;
    this.btnVal="show post";
  }

  toggle(){
    this.hide=!this.hide;
    if(!this.hide){
      this.btnVal="hide post"
    }
    else(this.btnVal="show post")
  }
    
}


@Component({                  
  selector: 'list',
  template:`
  <member-form (addMember)="newMember($event)"></member-form>
  <listItem *ngFor="let member of members" [member]="member">
    
    <span class="name">{{member.name}}</span>
    <h6 class="desig">{{member.desig}}</h6>
  
  </listItem>
  `
})
export class MembersListComponent{
  members:Member[];  
  // members are object of EonicsMemberItems class and not EonicsMembers class
  // EonicsMembers class objects is member-list

  constructor() {
    // the below values will get initialised and new objects of EonicsMemberItems will be created
    // and named as member in above ngFor loop 
    // as soon as <member-list> selector is detected 
    this.members=[  
    new Member("Abhinav Kumar","CEO",42),
    new Member("Navneet Kumar","Director",43),
    new Member("Nayan Singh","Trainee",20)
    ];
    console.log(this.members);
  }

  newMember(member){
    this.members.unshift(member);
    // this means when addMember function is emitted then newMember will be called
  }
  
  // toggle(member){
  //   member.hide=!member.hide
  // }
}