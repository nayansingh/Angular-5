import { Component, Input } from '@angular/core';
import {Member} from './list.component';

@Component({
    selector:'listItem',
    template:`
        <div class="card card-block">
            <h4 class="card-title">
                <ng-content select=".name"></ng-content>
                <h6>({{member.age}} years old)</h6>
            </h4>
            <p class="card-text" [hidden]="member.hide">
                <ng-content select=".desig"></ng-content>
            </p>
            <button class="btn btn-primary" 
            (click)="member.toggle()">{{member.btnVal}}</button>
        </div>
    `
})
export class MemberItemsComponent{
    @Input() member:Member;

    constructor(){
        console.log(`new member is ${this.member}`);
    }

    ngOnChanges(): void {
        //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
        //Add '${implements OnChanges}' to the class.
        console.log(`ngOnChanges - member is ${this.member}`);
    }

    ngOnInit(): void {
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
        console.log(`ng-onit -member is ${this.member}`);
    }

    ngDoCheck() {
        //Called every time that the input properties of a component or a directive are checked. Use it to extend change detection by performing a custom check.
        //Add 'implements DoCheck' to the class.
        console.log(`ng-docheck`);
    }

    ngAfterContentInit() {
        //Called after ngOnInit when the component's or directive's content has been initialized.
        //Add 'implements AfterContentInit' to the class.
        console.log(`ngAfterContentInit`);
    }

    ngAfterContentChecked() {
        //Called after every check of the component's or directive's content.
        //Add 'implements AfterContentChecked' to the class.
        console.log(`ngAfterContentChecked`);
    }

    ngAfterViewInit() {
        //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
        //Add 'implements AfterViewInit' to the class.
        console.log(`ngAfterContentInit`);
    }

    ngAfterViewChecked() {
        //Called after every check of the component's view. Applies to components only.
        //Add 'implements AfterViewChecked' to the class.
        console.log('ngAfterViewChecked');
    }

    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        console.log("ngOnDestroy");
    }
}

