import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { EonicsComponent } from './eonics.component';
import { MembersListComponent} from './members/list.component';
import { CounterComponent} from './counter/counter.component'
import { MemberItemsComponent} from './members/listItems.component';
import { BasicComponent } from './basic/basic.component';
import { EmployeeFormComponent } from './employeeform/employeeform.component';
import { PersonComponent } from './person/person.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MemberFormComponent } from './members/member-form.component';
import { PipesComponent } from './pipes/pipes.component';
import { ModelFormComponent } from './model-form/model-form.component';
 

@NgModule({
  declarations: [                 // all the components to be included in this module 
    EonicsComponent, 
    CounterComponent,
    MembersListComponent,
    MemberItemsComponent,
    BasicComponent,
    EmployeeFormComponent,
    PersonComponent,
    MemberFormComponent,
    PipesComponent,
    ModelFormComponent
  ],    
  imports: [      // needed to run module in browser and ngModel to work
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],            
  bootstrap: [EonicsComponent]           // contains all the bits and pieces to run app in the browser
})
export class EonicsModule {          // same as export class appModule
}
