import { Component } from '@angular/core';

@Component({
  selector: 'employee-form',
  templateUrl: './employeeform.view.html',
})
export class EmployeeFormComponent {
  text:string="enter text";
  enable:boolean=true;
  hide:boolean=true;
  keys:any=" ";
  txtVal:any;

  onclick(){
    this.enable=!this.enable;
    this.hide=!this.hide;
    this.text="new text";
  }

  onMouse(event:any){
    if(event.type=="mouseover") event.target.src="src/images/mclaren.jpg";
    else event.target.src="src/images/lambo.jpeg";
  }

  onkey(event:any){
    this.keys=event.key;
    this.txtVal=event.target.value;
  }

}
