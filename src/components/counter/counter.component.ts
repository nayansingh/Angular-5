import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'counter',
  template: `
      <button class="btn btn-danger" (click)="increase()">+</button>
      <button class="btn btn-warning" (click)="decrease()">-</button>
      <h5>from counter component <br>count:{{count}}</h5>
  `
})
export class CounterComponent{

  @Input() count:number=0;

  @Output() plus=new EventEmitter();
  @Output() minus=new EventEmitter();

  increase(){
    this.count++;
    this.plus.emit(this.count);
  }

  decrease(){
    this.count--;
    this.minus.emit(this.count);
  }
}
