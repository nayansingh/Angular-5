import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'person',
  templateUrl: './person.view.html',
})
export class PersonComponent {
  @Input() first:string;
  @Input() last:string;
  
  company:string="";
  post:string="";

  // person:Person = new Person("","");
}
