import { Component } from '@angular/core';
import { Employee,EmployeeType } from './employee.model';

@Component({
  selector: 'basic',
  templateUrl:`./basic.view.html`
})
export class BasicComponent {
  // count=2;
  count:number=7;
  modifyCount(count){
    // console.log(count);
    this.count=count;
  }

  course:string="Angular";
  time:Date=new Date();
  
  studying:boolean=false;
  faculty:string="Saurabh";
  specialization:boolean=false;
  duration:number=30;
  relatedCourses:object[]=[
    {
      "course":"React",
      "chapters":30,
      "teacher":"Swati",
      "color":"green",
      "class":"text-danger"
    },
    {
      "course":"React-Redux",
      "chapters":10,
      "teacher":"Aparna",
      "color":"blue",
      "class":"text-secondary"
    },
    {
      "course":"Vue",
      "chapters":32,
      "teacher":"Ayush",
      "color":"red",
      "class":"text-warning"
    },
    {
      "course":"HTML",
      "chapters":40,
      "teacher":"Navneet",
      "color":"yellow",
      "class":"text-primary"
    },
    {
      "course":"CSS",
      "chapters":35,
      "teacher":"Maneesh",
      "color":"orange",
      "class":"text-info"
    }
  ];

  employees:Employee[]=[    // employees of type Employee
    new Employee("A01","Abhinav Kumar","CEO",EmployeeType.permanent),
    new Employee("N01","Navneet Kumar","Director",EmployeeType.temporary),
    new Employee("N02","Nayan Singh","Soft Appr",EmployeeType.contract),
    new Employee("M01","Maneesh Pareehar","Soft Developer",EmployeeType.contract),
  ];

  selectedEmployee:Employee=this.employees[2];

  constructor(){
    console.log(this.employees);
    console.log(this.relatedCourses);
  }

}
