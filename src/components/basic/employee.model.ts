export class Employee {
    Id:string;
    Name:string;
    Post:string;
    Type:EmployeeType;

    constructor(id:string,name:string,po:string,type:EmployeeType){
        this.Id=id;
        this.Name=name;
        this.Post=po;
        this.Type=type;
    }
}

export enum EmployeeType{
    temporary,permanent,contract
}
