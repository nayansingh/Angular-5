import { Component } from '@angular/core';

@Component({                  // we have passed two param to App Component -> selector and template
  selector: 'eonics',
  template:`
  <list></list>     <br><br><br><br>
  <pipes></pipes>     <br><br><br><br>
  <model-form></model-form> <br><br><br><br>
  <basic></basic>
  `
})
export class EonicsComponent{                // same as export class EonicsComponent
}





/*
let root=new App();  // it will create root object and initialize App class, all the properties 
// of App class gets into root object
// and call the constructor as it is the first function to be called 
console.log(root.name);
console.log(root.desig);

we dont need these because already App class is initialised in index.html
*/