import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { EonicsModule } from './components/eonics.module'

// let obs=Rx.Observable.

platformBrowserDynamic().bootstrapModule(EonicsModule)
  .catch(err => console.log(err));
