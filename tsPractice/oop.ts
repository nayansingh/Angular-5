class Person{
    first:string
    last:string
    dob:Date                 // dob is of Date(inbuilt) class type

    constructor(first:string,last:string,dob:Date){
        this.first=first;
        this.last=last;
        this.dob=dob;
    }

    // multiple constructor or constructor overloading is not allowed in typescript
    // constructor(){          
    //     this.first="naman";
    // }

    show(){
        return `${this.first} ${this.last} was born in ${this.dob}`;
    }
}

let p1:Person=new Person("Nayan","Singh",new Date(1992,9,12));  // p1 is object of Person and of Person class type
// p1.first="Nayan";
// p1.last="Singh";
// p1.dob=new Date(1992,9,12);
console.log(p1.show());


class Car{
    constructor(private make:string,private model:string,private year:Date ){
        // the parameters will declare three variables at time of object creation
        // so we do not have to separately declare these variables
        // we can just now directly refer to them with this keyword
    }

    details(){
        return `I have a ${this.make} ${this.model} since ${this.year}`
    }

}

let car1:Car=new Car("Toyota","Camry",new Date(2003,6,26));

console.log(car1.details());