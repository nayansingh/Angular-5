var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Person = /** @class */ (function () {
    function Person(first, last) {
        this.first = first;
        this.last = last;
    }
    Object.defineProperty(Person.prototype, "fullName", {
        get: function () {
            return " " + this.first + " " + this.last;
        },
        enumerable: true,
        configurable: true
    });
    Person.prototype.show = function () {
        return "I am " + this.fullName + " born in ";
    };
    return Person;
}());
var Employee = /** @class */ (function (_super) {
    __extends(Employee, _super);
    function Employee(first, last, dob, dept, salary) {
        var _this = 
        // chaining constructor 
        _super.call(this, first, last) || this;
        _this.dob = dob;
        _this.dept = dept;
        _this.salary = salary;
        return _this;
    }
    Employee.prototype.show = function () {
        this.dept = "Backend";
        var str = _super.prototype.show.call(this);
        // even in chaining also we have to call person show because variables in person are 
        // private and cannot be accessed directly
        return str + " " + this.dob + " working in " + this.dept + " for " + this.salary;
    };
    return Employee;
}(Person));
var p1 = new Employee("Nayan", "Singh", new Date(1992, 9, 12), "Frontend", "Rs 10289");
console.log(p1.show());
