var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
var input = [1, 2, 3, 4, 5, 6];
var f = input[0], s = input[1], t = input[2], fo = input[3], fv = input[4], sx = input[5];
console.log(f, s, t, fo, fv, sx);
var words = ["nayan ", "singh ", "eonicslabs "].slice(0);
console.log(words[0]);
console.log(words);
var obj = {
    a: "red ",
    b: "blue ",
    c: "green "
};
var col1 = obj.a, col2 = obj.b, col3 = obj.c;
console.log(col1, col2, col3);
var colors = __rest(obj, []);
console.log(colors);
