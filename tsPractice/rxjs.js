let obs = Rx.Observable                      // instance of RxJS Observable
            .interval(1000)                  // this will continue infinitely unless we put a condition to stop it
            .take(4)                         // take allowes only 4 values from previous operator to go on
            .map(data => Date(Date.now()) ); // Date.now() gives number of miliseconds elapsed since Jan 1 1970

obs.subscribe(value => console.log(`Subscriber: ${value}`));   // subscribe takes the value of observable one by one from the end of observable chain       