class Person{
    private first:string    // accessible only inside defining class
    private last:string
    protected dob:Date      // accessible oin child class also

    constructor(first:string,last:string){
        this.first=first;
        this.last=last;
        
    }

    public get fullName():string{
        return ` ${this.first} ${this.last}`
    }

    show():string {
        return `I am ${this.fullName} born in `;
    }
}

class Employee extends Person{
    private dept:string;
    private salary:string;

    constructor(first:string,last:string,dob:Date,dept:string,salary:string){
        // chaining constructor 
        super(first,last);
        this.dob=dob;
        this.dept=dept;
        this.salary=salary;
    }

    show():string {
        this.dept="Backend";
        let str:string=super.show();
        // even in chaining also we have to call person show because variables in person are 
        // private and cannot be accessed directly
        return `${str} ${this.dob} working in ${this.dept} for ${this.salary}`;
    }
}

let p1:Employee=new Employee("Nayan","Singh",new Date(1992,9,12),"Frontend","Rs 10289");
console.log(p1.show());