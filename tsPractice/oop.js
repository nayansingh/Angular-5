var Person = /** @class */ (function () {
    function Person(first, last, dob) {
        this.first = first;
        this.last = last;
        this.dob = dob;
    }
    // multiple constructor or constructor overloading is not allowed in typescript
    // constructor(){          
    //     this.first="naman";
    // }
    Person.prototype.show = function () {
        return this.first + " " + this.last + " was born in " + this.dob;
    };
    return Person;
}());
var p1 = new Person("Nayan", "Singh", new Date(1992, 9, 12)); // p1 is object of Person and of Person class type
// p1.first="Nayan";
// p1.last="Singh";
// p1.dob=new Date(1992,9,12);
console.log(p1.show());
var Car = /** @class */ (function () {
    function Car(make, model, year) {
        this.make = make;
        this.model = model;
        this.year = year;
        // the parameters will declare three variables at time of object creation
        // so we do not have to separately declare these variables
        // we can just now directly refer to them with this keyword
    }
    Car.prototype.details = function () {
        return "I have a " + this.make + " " + this.model + " since " + this.year;
    };
    return Car;
}());
var car1 = new Car("Toyota", "Camry", new Date(2003, 6, 26));
console.log(car1.details());
