// console.log(`hello
// world: `);

// var arr=[];

// for(var i=0;i<10;i++){
//     var y=i*10;
//     arr.push(y)
// }

// arr.forEach(function(v,i,c){
//     console.log(i+" "+v+" "+c);
// })

// const foo=Object.freeze({});            // the variable is mutable not the value
// foo.a='a';
// console.log(foo);

// let add = (a,b) => a+b;

// console.log(add(4,5));

// let obj={
//     name:"Nayan",
//     sayLater:function(){
//         let self=this;
//         setTimeout(function(){
//             console.log(self.name)}         // here name out of context after 2 seconds
//             ,2000)   
//     }
// }

// obj.sayLater();

// let o={
//     name:"Nayan",
//     sayLater:function(){
//         setTimeout(()=>{                    
//             console.log(this.name)}         
//             ,2000)   
//     }
// }

// // fat arrow reserves the value of this it gives stability to this

// o.sayLater();


arr=['a','b','c'];

for(let val of arr){                // for of is used for arrays
    console.log(val);
}